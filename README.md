# cg-de-pon

## 説明

AI画像生成プロンプトまとめwiki [わがままAI CGでポン！](https://seesaawiki.jp/cg-de-pon/)の拡張jsなどを管理するリポジトリです。
コードは [novelai 5ch wiki拡張jsリポジトリ](https://github.com/naisd5ch/novel-ai-5ch-wiki-js) をコピー・改修しています。

## 配信方法

mainブランチがgitlab pagesで公開され、 wikiのフリーエリアのこのhtmlによりインポートされます。

```
<!--
    バグ報告、提案、PRはここまでお願いします。
    https://gitlab.com/senka-ai/cg-de-pon/
-->
<link href="https://senka-ai.gitlab.io/cg-de-pon/prompt-table.css" rel="stylesheet">
<script type="text/javascript" charset="utf-8" src="https://senka-ai.gitlab.io/cg-de-pon/prompt-table.js"></script>
```

## 自分も修正できるぜ！したいぜ！という人へ

GitLabのアカウントがあれば特に権限とかはなくできるはず。
修正の場合は履歴管理用に<s>プルリク</s>マーリク作ってもらえるといいかも。
専科の報告・要望をまとめたIssue作成だけでもありがたいです。

## デバッグ手順

たぶん以下がかんたん。

### 準備

- テスト用のSeesaa wiki
  - アカウントがあればつくれる
- ローカルで静的サイトをホストできるテストサーバ
  - なんでもいいけどホットリロードが効いたほうがいい
  - VS Code使ってるなら[Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)が使いやすい

### テストサーバ起動

- リポジトリのルートディレクトリで起動する
- http://(localhostアドレス):(ポート番号)/public/prompt-table.js でjsファイルが参照できればOK

### テスト用wikiの設定

- http://wiki-help.seesaa.net/article/360073171.html ここを参考にフリーエリアを１つ追加する
- サイドメニューの設定 > 追加したフリーエリアの操作 > 編集
- 設定画面で以下を入力して保存

```
<link href="http://(localhostアドレス):(ポート番号)/public/prompt-table.css" rel="stylesheet">
<script type="text/javascript" charset="utf-8" src="http://(localhostアドレス):(ポート番号)/public/prompt-table.js"></script>
```

### デバッグ手順

- テストサーバが起動していれば、ローカルのソースが反映された状態でテスト用wikiが動作する
- ソース変更した場合、ホットリードが効いていればwiki側をブラウザリロードするだけで反映される