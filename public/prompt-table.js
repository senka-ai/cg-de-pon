//@ts-check
//'senka_grid_input'classがついているseesaaのtableに機能追加

function senka_debug(data)
{
    const container = document.createElement('span');
    container.textContent = `${JSON.stringify(data, undefined, '\t')}`;
    container.append(document.createElement('br'));
    document.body.prepend(container);
}

/**
 * @enum {number}
 * @readonly
 */
ConvertPromptType = {
    WebUI: 0,
    NovelAI: 1,
};

class ConvertPrompt{
    /**
     * @typedef {Object} PromptElem
     * @property {string} text
     * @property {number} power
     */

    /**
     * @param {string} srcPrompt
     * @param {number} index
     * @return {[PromptElem, number]}
     */
    webuiPromptParse(srcPrompt, index)
    {
        let dstPrompt = '';
        let power = 1.0;

        //空白の読み飛ばし
        while(index < srcPrompt.length && srcPrompt[index] === ' ')
        {
            index++;
        }

        //強度を取る(数値指定がある場合は上書きされる)
        while(index < srcPrompt.length && (srcPrompt[index] === '(' || srcPrompt[index] === '['))
        {
            if(srcPrompt[index] === '(')
            {
                power *= 1.1;
            }
            if(srcPrompt[index] === '[')
            {
                power /= 1.1;
            }
            index++;
        }

        //記号が来るまで中身を追加していく
        while(index < srcPrompt.length &&
            srcPrompt[index] !== '[' && srcPrompt[index] !== ']' &&
            srcPrompt[index] !== '(' && srcPrompt[index] !== ')' &&
            srcPrompt[index] !== ':'
            )
        {
            //記号でなければ中身のプロンプトとする
            dstPrompt += srcPrompt[index];
            index++;
        }

        //コロンなら強度を読んで上書き
        if(index < srcPrompt.length && srcPrompt[index] === ':')
        {
            index++; //コロンの次に行く

            let powerText = '';
            while(index < srcPrompt.length && /^[\d.]$/.test(srcPrompt[index]))
            {
                powerText += srcPrompt[index];
                index++;
            }
            power = parseFloat(powerText);
        }

        //最後に閉じる記号を読み飛ばす
        while(index < srcPrompt.length && (srcPrompt[index] === ')' || srcPrompt[index] === ']' ||
            srcPrompt[index] === ' ' || srcPrompt[index] === ','))
        {
            index++;
        }

        return [{text: dstPrompt, power: power}, index];
    }

    /**
     * @param {string} srcPrompt
     * @param {number} index
     * @return {[PromptElem, number]}
     */
    naiPromptParse(srcPrompt, index)
    {
        let dstPrompt = '';
        let power = 1.0;

        //空白の読み飛ばし
        while(index < srcPrompt.length && srcPrompt[index] === ' ')
        {
            index++;
        }

        //強度を取る
        while(index < srcPrompt.length && (srcPrompt[index] === '{' || srcPrompt[index] === '['))
        {
            const char = srcPrompt[index];
            index++;
            if(char === ' ')
            {
                continue;
            }
            if(char === '{')
            {
                power *= 1.05;
                continue;
            }
            if(char === '[')
            {
                power /= 1.05;
                continue;
            }

            break;
        }

        //記号が来るまで中身を追加していく
        while(index < srcPrompt.length &&
            srcPrompt[index] !== '{' && srcPrompt[index] !== '}' &&
            srcPrompt[index] !== '[' && srcPrompt[index] !== ']')
        {
            //記号でなければ中身のプロンプトとする
            dstPrompt += srcPrompt[index];

            index++;
        }

        //最後に閉じる記号を読み飛ばす
        while(index < srcPrompt.length && (srcPrompt[index] === '}' || srcPrompt[index] === ']' ||
                srcPrompt[index] === ' ' || srcPrompt[index] === ',')
            )
        {
            index++;
        }

        return [{text: dstPrompt, power: power}, index];
    }


    /**
     * @param {PromptElem} prompt
     * @param {ConvertPromptType} promptType
     */
    elemToText(prompt, promptType)
    {
        let dstPrompt = '';
        if(promptType === ConvertPromptType.WebUI)
        {
            const toEscape = (text) =>{
                return text.replace(/[()\[\]]/g, match=>{
                    return `\\${match}`;
                });
            }
            //1だったらそのまま帰す
            if(prompt.power === 1.0)
            {
                return toEscape(prompt.text);
            }
            const powerText = prompt.power
                .toFixed(5)
                .replace(/0+$/, '')
                .replace(/\.$/, '.0');
            dstPrompt = `(${toEscape(prompt.text)}:${powerText}), `;
        }
        if(promptType === ConvertPromptType.NovelAI)
        {
            //1だったらそのまま帰す
            if(prompt.power === 1.0)
            {
                return prompt.text;
            }

            let powerCount = 0;
            //実際の値を超える最小のカッコの個数を求める
            while(true)
            {
                //長くなりすぎないよう、最大15までにしておく
                if(powerCount > 15)
                {
                    break;
                }

                powerCount++;
                if(prompt.power <= 1.0 && prompt.power > 1.0 / (1.05 ** powerCount))
                {
                    break;
                }else if(prompt.power >= 1.0 && prompt.power < 1.05 ** powerCount)
                {
                    break;
                }
            }

            //超える個数と超えない個数で、実際の値に近くなる方を採用
            if(powerCount > 0 && prompt.power <= 1.0)
            {
                if(Math.abs(prompt.power - 1.0 / (1.05 ** powerCount)) > Math.abs(prompt.power - 1.0 / (1.05 ** (powerCount - 1))))
                {
                    powerCount--;
                }
            }else if(powerCount > 0 && prompt.power >= 1.0)
            {
                if(Math.abs(prompt.power - (1.05 ** powerCount)) > Math.abs(prompt.power - (1.05 ** (powerCount - 1))))
                {
                    powerCount--;
                }
            }

            if(prompt.power > 1.0)
            {
                dstPrompt = `${'{'.repeat(powerCount)}${prompt.text}${'}'.repeat(powerCount)}, `;
            }else{
                dstPrompt = `${'['.repeat(powerCount)}${prompt.text}${']'.repeat(powerCount)}, `;
            }
        }
        return dstPrompt;
    }

    /**
     *
     * @param {string} srcPrompt
     * @param {ConvertPromptType} srcType
     * @param {ConvertPromptType} dstType
     */
    convert(srcPrompt, srcType, dstType){
        let dstPrompt = '';
        let index = 0;
        while(index < srcPrompt.length)
        {
            if(srcType === ConvertPromptType.WebUI)
            {
                const [prompt, newIndex] = this.webuiPromptParse(srcPrompt, index);
                dstPrompt += this.elemToText(prompt, dstType);
                index = newIndex;
            }
            if(srcType === ConvertPromptType.NovelAI)
            {
                const [prompt, newIndex] = this.naiPromptParse(srcPrompt, index);
                dstPrompt += this.elemToText(prompt, dstType);
                index = newIndex;
            }
        }
        return dstPrompt;
    }
}
{
    //プロンプト変換器を配置するdivにこれをつける
    const targetTableClass = "senka_prompt_convert";

    //変換可能
    const selectOptions = [[ConvertPromptType.NovelAI, 'NovelAI'], [ConvertPromptType.WebUI, 'WebUI']];

    //選択されたときに、他のSelectが選択しているoptionをかぶらないように別のやつに変えておく
    /**
     * @param {HTMLSelectElement} target
     * @param {HTMLSelectElement[]} selectNodes
     */
    const updateSelectPromptType = (target, selectNodes) => {
        for(const node of selectNodes)
        {
            if(node === target)
            {
                continue;
            }
            if(node.value !== target.value)
            {
                continue;
            }

            for(const option of selectOptions)
            {
                if(option[0] === parseInt(target.value))
                {
                    continue;
                }

                node.value = option[0];
            }
        }
    }

    /**
     * @param {HTMLElement} container
     */
    const updateTextArea = (container)=>{
        /** @type {HTMLTextAreaElement} */
        const srcTextArea = container.getElementsByClassName('senka_convert_src_textarea')[0];
        /** @type {HTMLSelectElement} */
        const srcPromptTypeSelect = container.getElementsByClassName('senka_src_type_select')[0];
        /** @type {HTMLTextAreaElement} */
        const dstTextArea = container.getElementsByClassName('senka_convert_dst_textarea')[0];
        /** @type {HTMLSelectElement} */
        const dstPromptTypeSelect = container.getElementsByClassName('senka_dst_type_select')[0];
        const converter = new ConvertPrompt();
        const prompt = converter.convert(
            srcTextArea.value,
            /** @type {ConvertPromptType} */ parseInt(srcPromptTypeSelect.value),
            /** @type {ConvertPromptType} */ parseInt(dstPromptTypeSelect.value)
        );
        dstTextArea.value = prompt;
    }


    const setupConvertComponent = () => {
        for (const node of document.getElementsByClassName(targetTableClass)) {
            const container = document.createElement('div');
            container.innerHTML = `
<div style="display: flex; align-items: center">
    <div style="display: flex; flex-flow: column; height: 400px; width: 500px">
        <select class="senka_convert_select senka_src_type_select"></select>
        <textarea class="senka_convert_src_textarea" style="height: 100%"></textarea>
    </div>
    <span style="font-size: 30px">▷</span>
    <div style="display: flex; flex-flow: column; height: 400px; width: 500px">
        <select class="senka_convert_select senka_dst_type_select"></select>
        <textarea class="senka_convert_dst_textarea" disabled style="height: 100%"></textarea>
    </div>
</div>
`;
            /** @type {HTMLSelectElement[]} */
            const selectNodes = [];
            for (const selectNode of container.getElementsByClassName('senka_convert_select')) {
                selectNodes.push(selectNode);
                for (const optionName of selectOptions) {
                    const optionElem = document.createElement('option');
                    optionElem.value = optionName[0];
                    optionElem.text = optionName[1];
                    selectNode.addEventListener('change', event => {
                        if(!(event.target instanceof HTMLSelectElement)){
                            return;
                        }
                        updateSelectPromptType(event.target, selectNodes);
                        updateTextArea(container);
                    });
                    selectNode.append(optionElem);
                }
            }
            updateSelectPromptType(selectNodes[0], selectNodes);

            /** @type {HTMLTextAreaElement} */
            const srcTextArea = container.getElementsByClassName('senka_convert_src_textarea')[0];
            srcTextArea.addEventListener('input', event => {
                updateTextArea(container);
            });
            node.append(container);
        }
    }

    const onMyLoadEvent = () => {
        setupConvertComponent();
    };

    document.addEventListener("DOMContentLoaded", onMyLoadEvent);
}
{
    /**
     * @readonly
     * @enum {number}
     */
    const ColumnType = {
        Text: 1,
        Prompt: 2,
        Image: 3,
        EditLink: 4
    };

    //対象のテーブル このクラスがついている
    const targetTableClass = "senka_prompt_table";

    //'追加'ボタンの識別用クラス名
    const columnPostButtonClass = "senka_prompt_table_column_post_button";

    //プロンプトのTableのCellに付くクラス名
    const promptTableCellClass = "senka_prompt_table_cell";

    // アップロード結果となるURLがここのノードに入る
    const uploadImageURLId = "senka-upload-image-url";

    /**
     * seesaaの記法に引っかからないようにする
     *  @param {string} value
     */
    const encodeColumnValue = (value) => {
        let encodedValue = value.replace(/[\(\[\|]/g, (match) => {
            //'[' '(' '|' はseesaaの記法とぶつかってしまうので、コードポイントでの表示
            return `&#${match.charCodeAt(0)};`;
        });
        return encodedValue;
    };


    /**
     * 編集ページをGetしてformを引っこ抜く
     * @param {string} editPagePath
     */
    const fetchFormNode = async (editPagePath) => {
        const domParser = new DOMParser();
        const textDecoder = new TextDecoder("euc-jp"); //EUC...

        const editPageBuffer = await (await fetch(editPagePath)).arrayBuffer();
        const editPageText = textDecoder.decode(editPageBuffer);
        const editPageDom = domParser.parseFromString(editPageText, "text/html");

        const formNode = /** @type {HTMLFormElement} */ (
            editPageDom.getElementById("wiki-form")
        );
        return formNode;
    };

    /**
     * 入力データをサーバーにpostしてページを更新させる
     * @param {string} editPagePath
     * @param {string[]} columnValues
     */
    const postColumnDataAsync = async (editPagePath, columnValues) => {
        const formNode = await fetchFormNode(editPagePath);
        const textAreaNode = /** @type {HTMLTextAreaElement} */ (
            formNode.querySelector("#content")
        );
        textAreaNode.value += `\n|${columnValues.join("|")}|`;

        //引っこ抜いたformを非表示でbodyに追加して即post
        formNode.style.cssText = "display: none";
        document.body.appendChild(formNode);
        formNode.submit();
    };

    /**
     * 画像アップロードapiに画像をpostし、パーマリンクをテキストエリアに設定する
     * @param {HTMLInputElement} inputElement
     */
    const uploadImage = async (inputElement, num) => {
        if(!inputElement || !inputElement.files || inputElement.files.length < 1){
            return;
        }
        const endpoint = `${document.getElementById("wiki-title")?.getElementsByTagName("a")[0].href}api/upload/`;

        const editpageurl = document.getElementById("wiki-menu")?.getElementsByClassName("edit")[0]?.getElementsByTagName("a")[0].href
        const wikipageId = /id=(?<pageid>[0-9]+)/.exec(editpageurl)?.groups?.pageid ?? "";

        const formData = new FormData();
        const file = inputElement.files[0];
        formData.append('file', file);
        formData.append('wikipage_id', wikipageId);
        const options = {
            method: 'POST',
            body: formData,
            headers: {
              'Content-Type': 'multipart/form-data',
              'accept': 'application/json, text/javascript, */*; q=0.01',

            },
          };
        delete options.headers['Content-Type'];

        // 設定したデータをPOST
        const res = await(await fetch(endpoint, options)).json();
        // 成功したらテキストエリアに設定
        if(res.success) {
            const urlArea = /** @type {HTMLTextAreaElement} */(document.getElementById(`${uploadImageURLId}-${num}`));
            if(urlArea !== null) {
                urlArea.value = res.attachment.permalink;
                alert("アップロードに成功しました。URLを画像入力エリアに設定します。");
            }
        } else {
            alert("アップロードに失敗したかも。編集画面から確認してみてください。");
        }
    }

    // 画像ファイル判定
    const isImage = (files) =>
        files.length > 0
     && /^image\/.+$/.test(files[0].type);

    // 画像アップロードゾーンを作成
    const createImageUploadNode = (num) => {
        const fileinput = document.createElement("input");
        fileinput.type = "file";
        fileinput.accept = "image/*";
        fileinput.addEventListener("change", () => uploadImage(fileinput, num));
        fileinput.classList.add("senka_image_file_input");

        const label = document.createElement("label");
        label.innerText = "クリックorファイルドロップで画像がアップロードできます"
        label.classList.add("senka_image_file_label");

        const dropzone = document.createElement("div");
        dropzone.setAttribute("dropzone", "copy f:image/png f:image/gif f:image/jpeg");
        dropzone.classList.add("senka_image_dropzone");

        dropzone.addEventListener("dragover", (e) => {
            dropzone.classList.add("senka_image_dropzone_over");
            e.preventDefault();
        });
        dropzone.addEventListener("dragleave", () => {
            dropzone.classList.remove("senka_image_dropzone_over");
        });
        dropzone.addEventListener("drop", (e) => {
            e.preventDefault();
            const files = e.dataTransfer?.files;
            if(files && isImage(files)) {
                fileinput.files = files;
                uploadImage(fileinput, num);
            }
            dropzone.classList.remove("senka_image_dropzone_over");
        });

        const container = document.createElement("div");
        container.classList.add("senka_image_dropzone_container");

        label.appendChild(fileinput);
        dropzone.appendChild(label);
        container.appendChild(dropzone);
        return container;
    }

    /**
     * @param {ColumnType[]} columnTypes
     */
    const createSenkaGridInputElement = (columnTypes, num) => {
        const node = document.createElement("tr");
        for (const column of columnTypes) {
            const tdElement = document.createElement("td");
            node.append(tdElement);

            let element;
            switch (column) {
                case ColumnType.Image:
                    element = document.createElement("textarea");
                    element.id = `${uploadImageURLId}-${num}`;
                    break;
                case ColumnType.Prompt:
                case ColumnType.Text:
                    element = document.createElement("textarea");
                    break;
                case ColumnType.EditLink:
                    element = document.createElement("button");
                    element.classList.add(columnPostButtonClass);
                    element.innerText = "Add";
                    break;
                default:
                    throw "範囲外";
            }
            tdElement.append(element);
        }

        return node;
    };

    /**
     * カラムのタイプを取得
     * @param {HTMLTableSectionElement} headElement
     */
    const getColumnTypes = (headElement) => {
        if (headElement.length === 0) {
            return undefined;
        }

        /** @type {ColumnType[]} */
        const columns = [];
        for (const node of headElement.rows[0].cells) {
            if (node.textContent?.includes("プロンプト")) {
                columns.push(ColumnType.Prompt);
                continue;
            } else if (node.textContent?.includes("サンプル") || node.textContent?.includes("画像")) {
                columns.push(ColumnType.Image);
                continue;
            } else if (node.classList.contains("table_edit_link")) {
                columns.push(ColumnType.EditLink);
                continue;
            } else {
                columns.push(ColumnType.Text);
            }
        }
        return columns;
    };

    /**
     *  簡単追加機能を追加
     * @param {HTMLTableElement} tableNode
     * @param {ColumnType[]} columnTypes
     */
    const setupAddHeader = (tableNode, columnTypes, num) => {
        const editLink = tableNode.tHead
            ?.querySelector(".table_edit_link a")
            ?.getAttribute("href");
        if (!editLink) {
            return;
        }

        const node = createSenkaGridInputElement(columnTypes, num);
        node.addEventListener("click", (event) => {
            if (!(event.target instanceof HTMLElement)) return;

            if (event.target.classList.contains(columnPostButtonClass)) {
                /** @type {string[]} */
                const columnValues = [];
                for (const columnNode of node.querySelectorAll("td")) {
                    const inputNode = columnNode.querySelector("textarea");
                    if (inputNode === null) {
                        continue;
                    }

                    const columnType = columnTypes[columnNode.cellIndex];
                    let value =
                        columnType === ColumnType.Prompt
                            ? encodeColumnValue(inputNode.value)
                            : inputNode.value;
                    value = value.trim();
                    value = value.replace(/\n/g, ""); //改行はすべて消す
                    columnValues.push(value);
                }
                postColumnDataAsync(editLink, columnValues);
            }
        });
        tableNode.tHead?.append(node);
        // 画像アップロードボタン追加
        tableNode.before(createImageUploadNode(num));
    };

    /**
     * コピーボタンを付けて、プロンプトをスクロールできるようにする
     * @param {HTMLTableElement} tableNode
     * @param {ColumnType[]} columnTypes
     */
    const setupPromptCellNode = (tableNode, columnTypes) => {
        for (const rowNode of tableNode.tBodies[0].rows) {
            for (const cellNode of rowNode.cells) {
                if (columnTypes.length <= cellNode.cellIndex) {
                    continue;
                }
                if (columnTypes[cellNode.cellIndex] !== ColumnType.Prompt) {
                    continue;
                }

                const divNode = document.createElement("div");
                divNode.classList.add(promptTableCellClass);

                const copyButtonNode = document.createElement("button");
                copyButtonNode.textContent = "Copy";

                const promptTextAreaNode = document.createElement("textarea");
                promptTextAreaNode.disabled = true;
                promptTextAreaNode.value = cellNode.innerText;

                divNode.append(copyButtonNode);
                divNode.append(promptTextAreaNode);
                cellNode.innerText = "";
                cellNode.append(divNode);

                copyButtonNode.addEventListener("click", () => {
                    navigator.clipboard.writeText(promptTextAreaNode.value);
                });
            }
        }
    };

    const setupTable = () => {
        const tableNodes = /** @type {HTMLCollectionOf<HTMLTableElement>} */ (
            document.body.getElementsByClassName(targetTableClass)
        );
        let i = 0;
        for (const tableNode of tableNodes) {
            if (tableNode.tHead === null) {
                continue;
            }

            const columnTypes = getColumnTypes(tableNode.tHead);
            if (columnTypes === undefined) {
                continue;
            }
            ++i;
            setupAddHeader(tableNode, columnTypes, i);
            //setupPromptCellNode(tableNode, columnTypes);
        }
    };

    const onMyLoadEvent = () => {
        setupTable();
    };

    document.addEventListener("DOMContentLoaded", onMyLoadEvent);
}
